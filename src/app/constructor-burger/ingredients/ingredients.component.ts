import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Ingredient} from "../ingredient.model";

@Component({
  selector: 'app-ingredients',
  templateUrl: './ingredients.component.html',
  styleUrls: ['./ingredients.component.css']
})
export class IngredientsComponent {
  @Input() ingredients: Ingredient[] = [];
  @Output() addIngredients = new EventEmitter();
  @Output() deleteIngredients = new EventEmitter();

  addIngredient(index: number) {
    this.ingredients[index].quantity++;
    const ingredient = new Ingredient(this.ingredients[index].name, this.ingredients[index].cost, this.ingredients[index].quantity, '')
    this.addIngredients.emit(ingredient);
  };

  deleteIngredient(index: number) {
    this.ingredients[index].quantity--;
    const ingredient = new Ingredient(this.ingredients[index].name, this.ingredients[index].cost, this.ingredients[index].quantity, '')
    this.deleteIngredients.emit(ingredient);
  };

}
