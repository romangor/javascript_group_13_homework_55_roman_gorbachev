import {Component, Input} from '@angular/core';
import {Ingredient} from "../ingredient.model";

@Component({
  selector: 'app-burger',
  templateUrl: './burger.component.html',
  styleUrls: ['./burger.component.css']
})
export class BurgerComponent  {
  @Input() ingredients: Ingredient[] = [];
  @Input() ingredientCH: Ingredient[] = [];
  @Input() ingredientMT: Ingredient[] = [];
  @Input() ingredientSD: Ingredient[] = [];
  @Input() ingredientBC: Ingredient[] = [];

  getTotalPrice(){
    let price = 20;
    this.ingredients.forEach(ingredient =>{
      price = price + ingredient.getPrice();
    });
    return price;
  }

}
