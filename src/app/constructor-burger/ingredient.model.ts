export class Ingredient {
  constructor(
    public name:string,
    public cost:number,
    public quantity:number,
    public url:string,
  ) {
  }

  getPrice(){
    return this.quantity * this.cost;
  }
}
